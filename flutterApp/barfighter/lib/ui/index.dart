import 'package:barfighter/ui/signin.dart';
import 'package:barfighter/ui/signup.dart';
import 'package:flutter/material.dart';

class Index extends StatefulWidget {
  @override
  State createState() => IndexState();
}

class IndexState extends State<Index> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          //contenedor con fondo de pantalla
          Container(
            decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: AssetImage('assets/images/bar.jpg'),
                    fit: BoxFit.cover)),
          ),

          Column(
              //alinear el centro
              mainAxisAlignment: MainAxisAlignment.start,
              //hijos de la columna
              children: <Widget>[
                //expanded coge el ancho entero ¿?
                Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          //titulo
                          Text(
                            'bar fighter',
                            style: TextStyle(
                                fontSize: 34.0,
                                fontFamily: 'Botsmatic Outline',
                                color: Colors.orange[900]),
                          ),

                          Padding(padding: EdgeInsets.only(top: 100.0)),

                          //row de botones
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            //botones
                            children: <Widget>[
                              //boton sign in
                              MaterialButton(
                                minWidth: 100,
                                height: 50,
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(50.0),
                                  side: BorderSide(color: Colors.orange[900]),
                                ),
                                child: Text(
                                  'Sign in',
                                  style: Theme.of(context).textTheme.body1,
                                ),
                                color: Colors.orange[900],
                                onPressed: () {
                                  //Navegacion a SignIn
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SignIn()));
                                },
                              ),

                              //boton sing up

                              MaterialButton(
                                minWidth: 100,
                                height: 50,
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(50.0),
                                  side: BorderSide(color: Colors.orange[900]),
                                ),
                                child: Text(
                                  'Sign up',
                                  style: TextStyle(color: Colors.orange[900]),
                                ),
                                color: Color.fromRGBO(0, 0, 0, 70),
                                onPressed: () {
                                  //Navegacion a SignUp
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SignUp()));
                                },
                              ),
                            ],
                          )
                        ],
                      ),
                    ))
              ]),
        ],
      ),
    );
  }
}
