import 'package:flutter/material.dart';

import 'package:barfighter/widgets/nav-drawer.dart';
import 'package:barfighter/ui/perfil.dart';
class Puntos extends StatefulWidget {
  @override
  State createState() => PuntosState();
}

class PuntosState extends State<Puntos> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(
          'tabla de puntos',
          style: TextStyle(
              fontSize: 25.0,
              fontFamily: 'Botsmatic Outline',
              color: Colors.grey[900]),
        ),
      actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Perfil()));
                  },
                  child: Icon(
                    Icons.person_outline,
                    size: 26.0,
                  ),
                ))
          ])
      ,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
              color: Colors.grey[800],
            ),
          ),
          Column(
              //alinear el centro
              mainAxisAlignment: MainAxisAlignment.start,
              //hijos de la columna
              children: <Widget>[
                Expanded(
                    child: Container(
                  padding: EdgeInsets.all(10),
                  child: new SingleChildScrollView(
                      child: new ConstrainedBox(
                    constraints: new BoxConstraints(),
                    child: new Column(children: <Widget>[
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Padding(padding: EdgeInsets.only(left: 6)),
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto',),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo',),
                            ),
                          ],
                        ),
                      ),
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto'),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo'),
                            ),
                          ],
                        ),
                      ),
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto'),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo'),
                            ),
                          ],
                        ),
                      ),
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto'),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo'),
                            ),
                          ],
                        ),
                      ),
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto'),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo'),
                            ),
                          ],
                        ),
                      ),
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto'),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo'),
                            ),
                          ],
                        ),
                      ),
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto'),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo'),
                            ),
                          ],
                        ),
                      ),
                      Card(
                        color: Colors.grey[700],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(children: <Widget>[
                              Image.asset(
                                'assets/images/alfombrilla.jpg',
                                height: 70,
                                width: 70,
                              ),
                              Padding(padding: EdgeInsets.only(left: 100)),
                              Text("10 pts.",
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: Colors.orange[900]))
                            ]),
                            //
                            const ListTile(
                              //leading: Icon(Icons.album, size: 50),
                              title: Text('Nombre Producto'),
                              subtitle: Text(
                                  'Lore ipsum dolor sit amet yabba dabba doo'),
                            ),
                          ],
                        ),
                      ),
                    ]),
                  )),
                )),
              ]),
        ],
      ),
    );
  }
}
