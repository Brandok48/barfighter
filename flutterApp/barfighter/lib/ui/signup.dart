import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'tienda.dart';
import 'package:http/http.dart' as http;
import 'package:barfighter/models/usuario.dart';
import 'package:barfighter/widgets/globals.dart' as global;

Future<Usuario> createUser(String nombre, String apellidos, String correo,
    String username, String pass, String tel) async {
  final response = await http.post(
    'http://' + global.ip + ':3000/usuario/addUser',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'nombre': nombre,
      'pass': pass,
      'apellidos': apellidos,
      'correo': correo,
      'nick': username,
      'num_tel': tel,
      'isPrivate': 'true',
    }),
  );

  if (response.statusCode == 200) {
    print(response.body+"bbbbbbb");
    return Usuario.fromJson(json.decode(response.body));
  } else if (response.statusCode == 201){
    print(response.body+"aaaaaa");
  }else{
    throw Exception('Failed to load user');
  }
}

class SignUp extends StatefulWidget {
  @override
  State createState() => SignUpState();
}

class SignUpState extends State<SignUp> {
  _showMyDialog() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Error'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Ese nombre de usuario ya existe'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Aceptar'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
  Usuario usu;
  bool existe = false;
  createUser(String nombre, String apellidos, String correo,
    String username, String pass, String tel) async {
  final response = await http.post(
    'http://' + global.ip + ':3000/usuario/addUser',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'nombre': nombre,
      'pass': pass,
      'apellidos': apellidos,
      'correo': correo,
      'nick': username,
      'num_tel': tel,
      'isPrivate': 'true',
    }),
  );

  if (response.statusCode == 200) {
    print(response.body+"bbbbbbb");
    usu = Usuario.fromJson(json.decode(response.body));
  } else if (response.statusCode == 201){
    existe = true;
    _showMyDialog();
    print(response.body+"aaaaaa");
  }else{
    throw Exception('Failed to load user');
  }
}
  final _usuController = TextEditingController();
  final _passController = TextEditingController();
  final _nomController = TextEditingController();
  final _apeController = TextEditingController();
  final _telController = TextEditingController();
  final _emailController = TextEditingController();
  Usuario u = new Usuario();
  Future<Usuario> _futureUSuario;
  List<String> value = [];

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    _usuController.dispose();
    _passController.dispose();
    _nomController.dispose();
    _apeController.dispose();
    _telController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var border = OutlineInputBorder(
      borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(40.0),
          topRight: const Radius.circular(40.0),
          bottomLeft: const Radius.circular(40.0),
          bottomRight: const Radius.circular(40.0)),
      borderSide: BorderSide(color: Colors.purple[200], width: 2.0),
    );
    var border2 = OutlineInputBorder(
      borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(40.0),
          topRight: const Radius.circular(40.0),
          bottomLeft: const Radius.circular(40.0),
          bottomRight: const Radius.circular(40.0)),
      borderSide: BorderSide(color: Colors.red, width: 3.0),
    );

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: AssetImage('assets/images/bar.jpg'),
                    fit: BoxFit.cover)),
          ),
          Column(
              //alinear el centro
              mainAxisAlignment: MainAxisAlignment.start,
              //hijos de la columna
              children: <Widget>[
                Expanded(
                    child: Container(
                  padding: EdgeInsets.all(10),
                  child: new SingleChildScrollView(
                      child: new ConstrainedBox(
                    constraints: new BoxConstraints(),
                    child: new Column(children: <Widget>[
                      Text(
                        'bar fighter',
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        width: 220,
                        child: TextField(
                          controller: _usuController,
                          style: TextStyle(color: Colors.orange),
                          decoration: InputDecoration(
                              focusedBorder: border,
                              enabledBorder: border2,
                              hintStyle: TextStyle(color: Colors.orange),
                              hintText: "Nombre de usuario"),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        width: 220,
                        child: TextField(
                          controller: _passController,
                          style: TextStyle(color: Colors.orange),
                          decoration: InputDecoration(
                              focusedBorder: border,
                              enabledBorder: border2,
                              hintStyle: TextStyle(color: Colors.orange),
                              hintText: "Contraseña"),
                              obscureText: true,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        width: 220,
                        child: TextField(
                          controller: _nomController,
                          style: TextStyle(color: Colors.orange),
                          decoration: InputDecoration(
                              focusedBorder: border,
                              enabledBorder: border2,
                              hintStyle: TextStyle(color: Colors.orange),
                              hintText: "Nombre"),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        width: 220,
                        child: TextField(
                          controller: _apeController,
                          style: TextStyle(color: Colors.orange),
                          decoration: InputDecoration(
                              focusedBorder: border,
                              enabledBorder: border2,
                              hintStyle: TextStyle(color: Colors.orange),
                              hintText: "apellidos"),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        width: 220,
                        child: TextField(
                          controller: _telController,
                          style: TextStyle(color: Colors.orange),
                          decoration: InputDecoration(
                              focusedBorder: border,
                              enabledBorder: border2,
                              hintStyle: TextStyle(color: Colors.orange),
                              hintText: "telefono"),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        width: 220,
                        child: TextField(
                          controller: _emailController,
                          style: TextStyle(color: Colors.orange),
                          decoration: InputDecoration(
                              focusedBorder: border,
                              enabledBorder: border2,
                              hintStyle: TextStyle(color: Colors.orange),
                              hintText: "E-mail"),
                        ),
                      ),
                      MaterialButton(
                        minWidth: 100,
                        height: 50,
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(50.0),
                          side: BorderSide(color: Colors.orange[900]),
                        ),
                        child: Text(
                          'Sign Up',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        color: Colors.orange[900],
                        onPressed: () async{
                          //Navegacion a SignIn
                          existe = false;
                          usu = null;
                          await createUser(
                                _nomController.text,
                                _apeController.text,
                                _emailController.text,
                                _usuController.text,
                                _passController.text,
                                _telController.text);
                          global.nick = _usuController.text;
                         if(existe == false)
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Tienda()));

                          print(_usuController.text);
                          print(_passController.text);
                          print(_nomController.text);
                          print(_apeController.text);
                          print(_telController.text);
                          print(_emailController.text);
                          setState(() {
                            //nombre,  apellidos,  correo, username,  pass,  tel
                            
                          });
                        },
                      ),
                    ]),
                  )),
                ))
              ]),
        ],
      ),
    );
  }
}
