import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:barfighter/ui/tienda.dart';
import 'package:barfighter/models/usuario.dart';
import 'package:barfighter/widgets/globals.dart' as global;

class SignIn extends StatefulWidget {
  @override
  State createState() => SignInState();
}

class SignInState extends State<SignIn> {
  _showMyDialog() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Error'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Usuario y contraseña no coinciden'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Aceptar'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
  Usuario _usuario;
  _fetchUsu(id) async {
    final response = await http
        .get('http://' + global.ip + ':3000/usuario/getUserByNick?nick=' + id);
    //print(response.body.substring(17,response.body.lastIndexOf('}')));
    
    if (response.body.indexOf('false') == -1) {
      print(response.body.indexOf('false'));
      _usuario = Usuario.fromJson(json.decode(response.body.substring(
          response.body.lastIndexOf('{'), response.body.lastIndexOf('}'))));
    } else {
      _usuario = null;
      print('failed to load Usuario');
    }
  }

  final _usuController = TextEditingController();
  final _passController = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    _usuController.dispose();
    _passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var border = OutlineInputBorder(
      borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(40.0),
          topRight: const Radius.circular(40.0),
          bottomLeft: const Radius.circular(40.0),
          bottomRight: const Radius.circular(40.0)),
      borderSide: BorderSide(color: Colors.purple[200], width: 2.0),
    );
    var border2 = OutlineInputBorder(
      borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(40.0),
          topRight: const Radius.circular(40.0),
          bottomLeft: const Radius.circular(40.0),
          bottomRight: const Radius.circular(40.0)),
      borderSide: BorderSide(color: Colors.red, width: 3.0),
    );

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: AssetImage('assets/images/bar.jpg'),
                    fit: BoxFit.cover)),
          ),
          Column(
              //alinear el centro
              mainAxisAlignment: MainAxisAlignment.start,
              //hijos de la columna
              children: <Widget>[
                Expanded(
                    child: Container(
                  padding: EdgeInsets.all(10),
                  child: new SingleChildScrollView(
                      child: new ConstrainedBox(
                    constraints: new BoxConstraints(),
                    child: new Column(children: <Widget>[
                      Text(
                        'bar fighter',
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                      Padding(padding: EdgeInsets.only(top: 100.0)),
                      Column(children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 12),
                          width: 220,
                          child: TextField(
                            controller: _usuController,
                            style: TextStyle(color: Colors.orange),
                            decoration: InputDecoration(
                                focusedBorder: border,
                                enabledBorder: border2,
                                hintStyle: TextStyle(color: Colors.orange),
                                hintText: "Nombre de usuario"),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 12),
                          width: 220,
                          child: TextField(
                            controller: _passController,
                            style: TextStyle(color: Colors.orange),
                            decoration: InputDecoration(
                                focusedBorder: border,
                                enabledBorder: border2,
                                hintStyle: TextStyle(color: Colors.orange),
                                hintText: "Contraseña"),
                            obscureText: true,
                          ),
                        )
                      ]),
                      Padding(padding: EdgeInsets.only(top: 100.0)),
                      MaterialButton(
                        minWidth: 100,
                        height: 50,
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(50.0),
                          side: BorderSide(color: Colors.orange[900]),
                        ),
                        child: Text(
                          'Sign in',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        color: Colors.orange[900],
                        onPressed: () async {
                          //Navegacion a SignIn
                          await _fetchUsu(_usuController.text);

                          //print(_usuario.nick);
                          //print(_usuController.text);

                          if (_usuario !=null &&_passController.text == _usuario.pass) {
                            global.nick = _usuController.text;
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Tienda()));
                          }
                          else{
                            _showMyDialog();
                            _passController.text="";
                          }
                        },
                      ),
                    ]),
                  )),
                ))
              ]),
        ],
      ),
    );
  }
}
