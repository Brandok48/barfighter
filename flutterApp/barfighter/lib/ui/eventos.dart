import 'dart:convert';

import 'package:barfighter/ui/DetallesEvento.dart';
import 'package:flutter/material.dart';

import 'package:barfighter/widgets/nav-drawer.dart';
import 'package:barfighter/ui/perfil.dart';
import 'package:http/http.dart' as http;
import 'package:barfighter/models/evento.dart';
import 'package:barfighter/widgets/globals.dart' as global;

class Eventos extends StatefulWidget {
  @override
  State createState() => EventosState();
}

class EventosState extends State<Eventos> {
  List<Evento> list = List();
  var isLoading = false;

  _fetchEvento() async {
    setState(() {
      isLoading = true;
    });
    final response = await http.get('http://' + global.ip + ':3000/evento/');

    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new Evento.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load evento');
    }
  }

  //Future<Evento> _eventoFuture;
  @override
  void initState() {
    super.initState();
    _fetchEvento();
    //_eventoFuture = fetchEvento();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
          title: Text(
            'bar fighter',
            style: TextStyle(
                fontSize: 34.0,
                fontFamily: 'Botsmatic Outline',
                color: Colors.grey[900]),
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Perfil()));
                  },
                  child: Icon(
                    Icons.person_outline,
                    size: 26.0,
                  ),
                ))
          ]),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  contentPadding: EdgeInsets.all(10.0),
                  title: new Text(list[index].nombre),
                  subtitle: new Text(list[index].descripcion),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: 44,
                      minHeight: 44,
                      maxWidth: 64,
                      maxHeight: 64,
                    ),
                    child: Image.asset('assets/images/banner.jpg',
                        height: 150, width: 150),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetallesEvento(
                                nombre: list[index].nombre,
                                descripcion: list[index].descripcion,
                                fecha: list[index].fecha)));
                  },
                );
              }),
    );
  }
}
