import 'package:barfighter/models/producto.dart';
import 'package:barfighter/widgets/globals.dart' as global;
import 'package:flutter/material.dart';

import 'package:barfighter/widgets/nav-drawer.dart';
import 'package:barfighter/ui/perfil.dart';

class DetallesProducto extends StatelessWidget {
  final String nombre, descripcion;
  final double precio;

  // receive data from the FirstScreen as a parameter
  DetallesProducto({Key key, this.nombre, this.descripcion, this.precio})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _showMyDialog(anadir) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Añadir al carrito?'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text("Desea añadir el producto al carrito?"),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Si'),
                onPressed: () {
                  global.carrito.add(anadir);
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
          title: Text(
            'bar fighter',
            style: TextStyle(
                fontSize: 34.0,
                fontFamily: 'Botsmatic Outline',
                color: Colors.grey[900]),
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Perfil()));
                  },
                  child: Icon(
                    Icons.person_outline,
                    size: 26.0,
                  ),
                ))
          ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          global.AnadirAlCarro anadir = global.AnadirAlCarro(
              nombre: nombre, descripcion: descripcion, precio: precio);
          // Add your onPressed code here!
          print(anadir.descripcion);
          
          _showMyDialog(anadir);
        },
        child: Icon(Icons.add_shopping_cart),
        backgroundColor: Colors.orange,
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
              color: Colors.grey[800],
            ),
          ),
          Column(
              //alinear el centro
              mainAxisAlignment: MainAxisAlignment.start,
              //hijos de la columna
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.all(100),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          //titulo
                          Image.asset('assets/images/camiseta.jpg',
                              height: 150, width: 150),
                          Text(
                            'producto:',
                            style: TextStyle(
                                fontSize: 24.0, color: Colors.orange[900]),
                          ),
                          Text(
                            nombre,
                            style:
                                TextStyle(fontSize: 24.0, color: Colors.white),
                          ),
                          Text(
                            'descripcion:',
                            style: TextStyle(
                                fontSize: 24.0, color: Colors.orange[900]),
                          ),
                          Text(
                            descripcion,
                            style:
                                TextStyle(fontSize: 24.0, color: Colors.white),
                          ),
                          Text(
                            precio.toString() + '€',
                            style:
                                TextStyle(fontSize: 24.0, color: Colors.white),
                          ),
                        ],
                      ),
                    )),
              ]),
        ],
      ),
    );
  }
}
