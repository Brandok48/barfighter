import 'package:flutter/material.dart';
import 'package:barfighter/widgets/nav-drawer.dart';
import 'package:barfighter/ui/perfil.dart';

class Salas extends StatefulWidget {
  @override
  State createState() => SalasState();
}

class SalasState extends State<Salas> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
          title: Text(
            'bar fighter',
            style: TextStyle(
                fontSize: 34.0,
                fontFamily: 'Botsmatic Outline',
                color: Colors.grey[900]),
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Perfil()));
                  },
                  child: Icon(
                    Icons.person_outline,
                    size: 26.0,
                  ),
                ))
          ]),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
              color: Colors.grey[800],
            ),
          ),
          Column(
              //alinear el centro
              mainAxisAlignment: MainAxisAlignment.start,
              //hijos de la columna
              children: <Widget>[
                Expanded(
                    child: Container(
                  padding: EdgeInsets.all(10),
                  child: new SingleChildScrollView(
                      child: new ConstrainedBox(
                    constraints: new BoxConstraints(),
                    child: new Column(children: <Widget>[
                      Text(
                        "pc master race",
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                      Padding(padding: EdgeInsets.only(top: 50)),
                      Text(
                        "roleplay one",
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                      Padding(padding: EdgeInsets.only(top: 50)),
                      Text(
                        "roleplay two",
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                      Padding(padding: EdgeInsets.only(top: 50)),
                      Text(
                        "xbox room",
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                      Padding(padding: EdgeInsets.only(top: 50)),
                      Text(
                        "pc master race",
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                      Padding(padding: EdgeInsets.only(top: 50)),
                      Text(
                        "pc master race",
                        style: TextStyle(
                            fontSize: 34.0,
                            fontFamily: 'Botsmatic Outline',
                            color: Colors.orange[900]),
                      ),
                    ]),
                  )),
                ))
              ]),
        ],
      ),
    );
  }
}
/*Expanded(
   child: Column(children: <Widget>[
   for (var item in items) item
])),*/
