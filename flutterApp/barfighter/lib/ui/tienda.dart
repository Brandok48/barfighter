import 'dart:convert';

import 'package:barfighter/models/producto.dart';
import 'package:barfighter/ui/DetallesProducto.dart';
import 'package:flutter/material.dart';

import 'package:barfighter/widgets/nav-drawer.dart';
import 'package:barfighter/ui/perfil.dart';
import 'package:http/http.dart' as http;
import 'package:barfighter/widgets/globals.dart' as global;

class Tienda extends StatefulWidget {
  @override
  State createState() => TiendaState();
}

class TiendaState extends State<Tienda> {
  List<Producto> list;
  var isLoading = false;
_showMyDialog(cart) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Confirmar carrito?'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(cart),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Confirmar pedido?'),
            onPressed: () {
              global.carrito = [];
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text('Cancelar'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
  _fetchProducto() async {
    setState(() {
      isLoading = true;
    });
    final response = await http.get('http://' + global.ip + ':3000/producto/');

    if (response.statusCode == 200) {
      //print(json.decode(response.body).toString());
      list = (json.decode(response.body) as List)
          .map((data) => new Producto.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load evento');
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchProducto();
    //_eventoFuture = fetchEvento();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          title: Text(
            'bar fighter',
            style: TextStyle(
                fontSize: 34.0,
                fontFamily: 'Botsmatic Outline',
                color: Colors.grey[900]),
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Perfil()));
                  },
                  child: Icon(
                    Icons.person_outline,
                    size: 26.0,
                  ),
                ))
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // Add your onPressed code here!
            String cart ='';
            if(global.carrito!=null)
            for (var item in global.carrito) {
              print(item.nombre);
              cart+=item.nombre+"\n";
            }
            _showMyDialog(cart);
          },
          child: Icon(Icons.shopping_cart),
          backgroundColor: Colors.orange,
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : GridView.builder(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 44,
                        minHeight: 44,
                        maxWidth: 64,
                        maxHeight: 64,
                      ),
                      child: Image.asset('assets/images/camiseta.jpg',
                          height: 150, width: 150),
                    ),
                    contentPadding: EdgeInsets.all(10.0),
                    title: new Text(list[index].nombre),
                    subtitle: new Text(list[index].descripcion),
                    onTap: () {
                      //print(list[index].fecha);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetallesProducto(
                                    nombre: list[index].nombre,
                                    descripcion: list[index].descripcion,
                                    precio:list[index].precio
                                  )));
                    },
                  );
                },
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
              ));
  }
}
