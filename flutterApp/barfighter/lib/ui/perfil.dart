import 'dart:convert';

import 'package:barfighter/models/usuario.dart';
import 'package:flutter/material.dart';

import 'package:barfighter/widgets/nav-drawer.dart';
import 'package:http/http.dart' as http;
import 'package:barfighter/widgets/globals.dart' as global;

class Perfil extends StatefulWidget {
  @override
  State createState() => PerfilState();
}

class PerfilState extends State<Perfil> {
  Usuario _usuario;
  var isLoading = false;
  _fetchUsu(id) async {
    setState(() {
      isLoading = true;
    });
    final response = await http
        .get('http://' + global.ip + ':3000/usuario/getUserByNick?nick=' + id);
    //print(response.body.substring(17,response.body.lastIndexOf('}')));
    if (response.statusCode == 200) {
      _usuario = Usuario.fromJson(json.decode(response.body.substring(
          response.body.lastIndexOf('{'), response.body.lastIndexOf('}'))));
      setState(() {
        isLoading = false;
      });
    } else {
      print('failed to load Usuario');
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchUsu(global.nick);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
          title: Text(
            'perfil',
            style: TextStyle(
                fontSize: 34.0,
                fontFamily: 'Botsmatic Outline',
                color: Colors.grey[900]),
          ),
          ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                    color: Colors.grey[800],
                  ),
                ),
                Column(
                    //alinear el centro
                    mainAxisAlignment: MainAxisAlignment.start,
                    //hijos de la columna
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(
                                left: 60.0,
                              )),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                          fit: BoxFit.fill,
                                          image: AssetImage(
                                              "assets/images/sonic.jpg")))),
                              Padding(padding: EdgeInsets.only(left: 10.0)),
                              Column(
                                children: <Widget>[
                                  Text(
                                    _usuario.nick,
                                    style: TextStyle(color: Colors.orange),
                                  ),
                                  Text(
                                    "infinite pts.",
                                    style: TextStyle(color: Colors.grey),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      Expanded(
                        // padding: EdgeInsets.all(100),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: new SingleChildScrollView(
                                    child: new ConstrainedBox(
                                        constraints: new BoxConstraints(),
                                        child: new Column(children: <Widget>[
                                          Column(children: <Widget>[
                                            //for (var item in items) item
                                            Container(
                                              margin: EdgeInsets.only(top: 12),
                                              width: 220,
                                              child: TextField(
                                                decoration: InputDecoration(
                                                    hintStyle: TextStyle(
                                                        color: Colors.orange),
                                                    hintText: _usuario.nick),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 12),
                                              width: 220,
                                              child: TextField(
                                                decoration: InputDecoration(
                                                    hintStyle: TextStyle(
                                                        color: Colors.orange),
                                                    hintText:
                                                        'Cambiar contraseña'),
                                                obscureText: true,
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 12),
                                              width: 220,
                                              child: TextField(
                                                decoration: InputDecoration(
                                                    hintStyle: TextStyle(
                                                        color: Colors.orange),
                                                    hintText: _usuario.nombre),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 12),
                                              width: 220,
                                              child: TextField(
                                                decoration: InputDecoration(
                                                    hintStyle: TextStyle(
                                                        color: Colors.orange),
                                                    hintText:
                                                        _usuario.apellidos),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 12),
                                              width: 220,
                                              child: TextField(
                                                decoration: InputDecoration(
                                                    hintStyle: TextStyle(
                                                        color: Colors.orange),
                                                    hintText: _usuario.numtel),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 12),
                                              width: 220,
                                              child: TextField(
                                                decoration: InputDecoration(
                                                    hintStyle: TextStyle(
                                                        color: Colors.orange),
                                                    hintText: _usuario.correo),
                                              ),
                                            ),
                                          ])
                                        ])))),
                          ],
                        ),
                      ),
                    ]),
              ],
            ),
    );
  }
}
