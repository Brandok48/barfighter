import 'package:flutter/material.dart';

import 'package:barfighter/widgets/nav-drawer.dart';
import 'package:barfighter/ui/perfil.dart';
class Chat extends StatefulWidget {
  @override
  State createState() => ChatState();
}

class ChatState extends State<Chat> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(
          'bar fighter',
          style: TextStyle(
              fontSize: 34.0,
              fontFamily: 'Botsmatic Outline',
              color: Colors.grey[900]),
        ),
      actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Perfil()));
                  },
                  child: Icon(
                    Icons.person_outline,
                    size: 26.0,
                  ),
                ))
          ]),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
              color: Colors.grey[800],
                ),
          ),
          Column(
              //alinear el centro
              mainAxisAlignment: MainAxisAlignment.start,
              //hijos de la columna
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.all(100),
                      child: Column(
                        
                        mainAxisAlignment: MainAxisAlignment.start,
                        
                        children: <Widget>[
                          //titulo
                          Text('Chat')
                        ],
                      ),
                    )),
                    
              ]),
        ],
      ),
    );
  }
}
