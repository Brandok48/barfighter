import 'package:flutter/material.dart';
import 'ui/index.dart';

void main() {
  runApp(new MaterialApp(
    theme: ThemeData(
        primaryColor: Colors.orange[900],
        accentColor: Colors.orangeAccent[400]),
    debugShowCheckedModeBanner: false,
    home: Index(),
  ));
}
