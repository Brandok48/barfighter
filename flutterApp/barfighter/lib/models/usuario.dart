class Usuario {
  
  final String nombre,pass,apellidos,correo,nick,numtel;

  Usuario({this.nombre, this.pass, this.apellidos,this.correo,this.nick,this.numtel});

  factory Usuario.fromJson(Map<String, dynamic> json) {
    return Usuario(
      nombre: json['nombre'],
      pass: json['pass'],
      apellidos: json['apellidos'],
      correo: json['correo'],
      nick: json['nick'],
      numtel: json['num_tel'],
      
    );
  }
}