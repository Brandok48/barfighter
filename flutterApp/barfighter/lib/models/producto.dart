class Producto {
  final String nombre, descripcion;
  final int puntos;
  final double precio;

  Producto({this.nombre, this.descripcion, this.puntos, this.precio});

  factory Producto.fromJson(Map<String, dynamic> json) {
    return Producto(
      nombre: json['nombre'],
      descripcion: json['descripcion'],
      puntos: json['puntos'],
      precio: json['precio'],
    );
  }
}
