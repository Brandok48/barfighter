class Evento {

  final String nombre,descripcion,fecha;

  Evento({this.nombre, this.descripcion, this.fecha});

  factory Evento.fromJson(Map<String, dynamic> json) {
    return Evento(
      nombre: json['nombre'],
      descripcion: json['descripcion'],
      fecha: json['fecha'],
    );
  }
}