import 'package:flutter/material.dart';
import 'package:barfighter/ui/chat.dart';
import 'package:barfighter/ui/eventos.dart';
import 'package:barfighter/ui/puntos.dart';
import 'package:barfighter/ui/salas.dart';
import 'package:barfighter/ui/perfil.dart';
import 'package:barfighter/ui/tienda.dart';
class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'BarFighter',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Perfil'),
            onTap: () {Navigator.of(context).pop();Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Perfil()));},
          ),
          ListTile(
            leading: Icon(Icons.shop),
            title: Text('Tienda'),
            onTap: ()  {Navigator.of(context).pop();Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Tienda()));},
          ),
          ListTile(
            leading: Icon(Icons.branding_watermark),
            title: Text('Salas'),
            onTap: ()  {Navigator.of(context).pop();Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Salas()));},
          ),
          ListTile(
            leading: Icon(Icons.apps),
            title: Text('Tabla de puntos'),
            onTap: ()  {Navigator.of(context).pop();Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Puntos()));},
          ),
          ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text('Eventos'),
            onTap: ()  {Navigator.of(context).pop();Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Eventos()));},
          ),
          ListTile(
            leading: Icon(Icons.chat_bubble),
            title: Text('Chat'),
            onTap: ()  {Navigator.of(context).pop();Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Chat()));},
          ),
        ],
      ),
    );
  }
}
