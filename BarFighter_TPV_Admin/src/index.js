const { BrowserWindow } = require("electron").remote;

const axios = require("axios");

const tpvBtn = document.getElementById("tpvDiv");
let adminBtn = document.getElementById("adminDiv");

//TPV window
tpvBtn.addEventListener("click", e => {
  tpvWin = new BrowserWindow({
    width: 400,
    height: 200,
    frame: true,
    alwaysOnTop: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  tpvWin.maximize();
  // tpvWin.removeMenu();
  //tpvWin.webContents.openDevTools();

  tpvWin.on("close", () => {
    tpvWin = null;
  });

  tpvWin.loadFile("./src/tpv/index.html");
  tpvWin.show();
});

//ADMIN window
adminBtn.addEventListener("click", e => {
  adminWin = new BrowserWindow({
    width: 400,
    height: 200,
    frame: true,
    alwaysOnTop: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  adminWin.maximize();

  // adminWin.webContents.openDevTools();

  adminWin.on("close", () => {
    adminWin = null;
  });

  adminWin.loadFile("./src/admin/index.html");
  adminWin.show();
});
