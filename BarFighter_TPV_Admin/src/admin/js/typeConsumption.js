ipc = require('electron').ipcRenderer;

var idConsumptionType = null
var nombre = null

async function printDataTipoConsumiciones() {
  var response = await axios.get("http://213.195.104.135:3000/tipoConsumicion");
  
  var table = document.querySelector(".datosTabla");
  data = response.data;
  
  data.forEach(element => {
    newRow = table.insertRow(table.rows.length);
    newRow.setAttribute("id", "element" + table.rows.length);
    cellNombre = newRow.insertCell();
    cellNombre.innerHTML = element.nombre;
    cellEdit = newRow.insertCell();
    cellEdit.innerHTML =
      "<img src='../../assets/icon/edit-regular.svg' alt='edit' />";
    cellEdit.setAttribute("class", "editItem");
    cellEdit.setAttribute("onclick", "editItem('ConsumptionType', '" + element._id + "')");
    cellDel = newRow.insertCell();
    cellDel.innerHTML =
      "<img src='../../assets/icon/trash-alt-regular.svg' alt='del' />";
    cellDel.setAttribute("class", "editItem");
    cellDel.setAttribute("onclick", "deltipoConsumicion('" + element._id + "')");
    table.append(newRow);
  });
}

ipc.on('modConsumptionType', async (event, id) => {
  var response = await axios.get("http://213.195.104.135:3000/tipoConsumicion/getTypeConsumption", {
    params: {
      id: id
    }
  });

  idConsumptionType = response.data.doc._id
  nombre = response.data.doc.nombre

  document.getElementById("nombre").value = nombre

})

async function addTypeConsumption() {
  var response = await axios.post("http://213.195.104.135:3000/tipoConsumicion/addTypeConsumption", {
    nombre: document.getElementById("nombre").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Tipo de consumicion creado",
      text: "El tipo de consumicion ha sido creado con exito"
    }).then(() => {
      window.close();
    });
  }
}

async function editTypeConsumption() {
  var response = await axios.put("http://213.195.104.135:3000/tipoConsumicion/modTypeConsumption", {
    id: idConsumptionType,
    nombre: document.getElementById("nombre").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Tipo de consumicion modificado",
      text: "El tipo de consumicion ha sido modificado con exito"
    }).then(() => {
      window.close();
    });
  }
}

function deltipoConsumicion(id) {
  swal({
    title: "¿Desea eliminar el producto?",
    text: "La consumicion seleccionado se eliminará",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Sí",
    cancelButtonText: "No",
    reverseButtons: true
  }).then(async willDelete => {
    if (willDelete) {
      var isDeleted;
      var response = await axios.delete("http://213.195.104.135:3000/tipoConsumicion/delTypeConsumption", {
        params: {
          id: id
        }
      });
  
      if(response.data.ok) {
        swal("Eliminado!", "el tipo de consumicion se ha eliminado!", "success");
      }
      else{
        swal("Error", "error al eliminar el tipo de producto", "error");
      }      
    }
  });  
}

function cancel() {
  document.getElementById("nombre").value = nombre
}