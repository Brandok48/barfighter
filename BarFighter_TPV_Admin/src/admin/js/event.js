ipc = require('electron').ipcRenderer;

var idEvento = null
var nombre = null
var descripcion = null
var fecha = null

async function printDataEventos() {
  var response = await axios.get("http://213.195.104.135:3000/evento");
  
  var table = document.querySelector(".datosTabla");
  data = response.data;
  
  data.forEach(element => {
    newRow = table.insertRow(table.rows.length);
    newRow.setAttribute("id", "element" + table.rows.length);
    cellNombre = newRow.insertCell();
    cellNombre.innerHTML = element.nombre;
    cellDescripcion = newRow.insertCell();
    cellDescripcion.innerHTML = element.descripcion;
    cellFecha = newRow.insertCell();
    cellFecha.innerHTML = element.fecha;
    cellEdit = newRow.insertCell();
    cellEdit.innerHTML =
      "<img src='../../assets/icon/edit-regular.svg' alt='edit' />";
    cellEdit.setAttribute("class", "editItem");
    cellEdit.setAttribute("onclick", "editItem('Event', '" + element._id + "')");
    cellDel = newRow.insertCell();
    cellDel.innerHTML =
      "<img src='../../assets/icon/trash-alt-regular.svg' alt='del' />";
    cellDel.setAttribute("class", "editItem");
    cellDel.setAttribute("onclick", "delEvento('" + element._id + "')");
    table.append(newRow);
  });
}

ipc.on('modEvent', async (event, id) => {
  var response = await axios.get("http://213.195.104.135:3000/evento/getEvent", {
    params: {
      id: id
    }
  });

  idEvento = response.data.doc._id
  nombre = response.data.doc.nombre
  descripcion = response.data.doc.descripcion
  fecha = response.data.doc.fecha

  document.getElementById("nombre").value = nombre
  document.getElementById("descripcion").value = descripcion
  document.getElementById("fecha").value = fecha

})

async function addEvent() {
  var response = await axios.post("http://213.195.104.135:3000/evento/addEvent", {
    nombre: document.getElementById("nombre").value,
    descripcion: document.getElementById("descripcion").value,
    fecha: document.getElementById("fecha").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Evento creado",
      text: "El evento ha sido creado con exito"
    }).then(() => {
      window.close();
    });
  }
}

async function editEvent() {
  var response = await axios.put("http://213.195.104.135:3000/evento/modEvent", {
    id: idEvento,
    nombre: document.getElementById("nombre").value,
    descripcion: document.getElementById("descripcion").value,
    fecha: document.getElementById("fecha").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Evento de producto modificado",
      text: "El evento ha sido modificado con exito"
    }).then(() => {
      window.close();
    });
  }
}

function delEvento(id) {
  swal({
    title: "¿Desea eliminar el producto?",
    text: "El producto seleccionado se eliminará",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Sí",
    cancelButtonText: "No",
    reverseButtons: true
  }).then(async willDelete => {
    if (willDelete) {
      var isDeleted;
      var response = await axios.delete("http://213.195.104.135:3000/evento/delEvent", {
        params: {
          id: id
        }
      });
  
      if(response.data.ok) {
        swal("Eliminado!", "el evento se ha eliminado!", "success");
      }
      else{
        swal("Error", "error al eliminar el evento", "error");
      }      
    }
  });  
}

function cancel() {
  document.getElementById("nombre").value = nombre
}