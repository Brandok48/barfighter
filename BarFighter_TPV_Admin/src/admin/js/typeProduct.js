ipc = require('electron').ipcRenderer;

var idProductType = null
var nombre = null

async function printDataTipoProductos() {
  var response = await axios.get("http://213.195.104.135:3000/tipoProducto");
  
  var table = document.querySelector(".datosTabla");
  data = response.data;
  
  data.forEach(element => {
    newRow = table.insertRow(table.rows.length);
    newRow.setAttribute("id", "element" + table.rows.length);
    cellNombre = newRow.insertCell();
    cellNombre.innerHTML = element.nombre;
    cellEdit = newRow.insertCell();
    cellEdit.innerHTML =
      "<img src='../../assets/icon/edit-regular.svg' alt='edit' />";
    cellEdit.setAttribute("class", "editItem");
    cellEdit.setAttribute("onclick", "editItem('ProductType', '" + element._id + "')");
    cellDel = newRow.insertCell();
    cellDel.innerHTML =
      "<img src='../../assets/icon/trash-alt-regular.svg' alt='del' />";
    cellDel.setAttribute("class", "editItem");
    cellDel.setAttribute("onclick", "delTipoProducto('" + element._id + "')");
    table.append(newRow);
  });
}

ipc.on('modProductType', async (event, id) => {
  var response = await axios.get("http://213.195.104.135:3000/tipoProducto/getTypeProduct", {
    params: {
      id: id
    }
  });

  idProductType = response.data.doc._id
  nombre = response.data.doc.nombre

  document.getElementById("nombre").value = nombre

})

async function addTypeProduct() {
  var response = await axios.post("http://213.195.104.135:3000/tipoProducto/addTypeProduct", {
    nombre: document.getElementById("nombre").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Tipo de producto creado",
      text: "El tipo de producto ha sido creado con exito"
    }).then(() => {
      window.close();
    });
  }
}

async function editTypeProduct() {
  var response = await axios.put("http://213.195.104.135:3000/tipoProducto/modTypeProduct", {
    id: idProductType,
    nombre: document.getElementById("nombre").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Tipo de producto modificado",
      text: "El tipo de producto ha sido modificado con exito"
    }).then(() => {
      window.close();
    });
  }
}

function delTipoProducto(id) {
  swal({
    title: "¿Desea eliminar el producto?",
    text: "El producto seleccionado se eliminará",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Sí",
    cancelButtonText: "No",
    reverseButtons: true
  }).then(async willDelete => {
    if (willDelete) {
      var isDeleted;
      var response = await axios.delete("http://213.195.104.135:3000/tipoProducto/delTypeProduct", {
        params: {
          id: id
        }
      });
  
      if(response.data.ok) {
        swal("Eliminado!", "el tipo de producto se ha eliminado!", "success");
      }
      else{
        swal("Error", "error al eliminar el tipo de producto", "error");
      }      
    }
  });  
}

function cancel() {
  document.getElementById("nombre").value = nombre
}