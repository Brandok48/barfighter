var ipc = require('electron').ipcRenderer;

var idConsumi = null
var nombre = null
var id_tipo = null
var puntos = 0
var precio = 0

async function printDataConsumiciones() {
  var response = await axios.get("http://213.195.104.135:3000/consumicion");
  
  var table = document.querySelector(".datosTabla");
  data = response.data;
  
  data.forEach(element => {
    newRow = table.insertRow(table.rows.length);
    newRow.setAttribute("id", "element" + table.rows.length);
    cellNombre = newRow.insertCell();
    cellNombre.innerHTML = element.nombre;
    cellPuntos = newRow.insertCell();
    cellPuntos.innerHTML = element.puntos;
    cellPrecio = newRow.insertCell();
    cellPrecio.innerHTML = element.precio + " €";

    cellEdit = newRow.insertCell();
    cellEdit.innerHTML =
      "<img src='../../assets/icon/edit-regular.svg' alt='edit' />";
    cellEdit.setAttribute("class", "editItem");
    cellEdit.setAttribute("onclick", "editItem('Consumption', '" + element._id + "')");
    cellDel = newRow.insertCell();
    cellDel.innerHTML =
      "<img src='../../assets/icon/trash-alt-regular.svg' alt='del' />";
    cellDel.setAttribute("class", "editItem");
    cellDel.setAttribute("onclick", "delConsumicion('" + element._id + "')");
    table.append(newRow);
  });
}

ipc.on('modConsumption', async (event, id) => {
  console.log(id);
    var response = await axios.get("http://213.195.104.135:3000/consumicion/getConsumption", {
      params: {
        id: id
      }
    });

    idConsumi = response.data.doc._id
    nombre = response.data.doc.nombre
    id_tipo = response.data.doc.id_tipo
    puntos = response.data.doc.puntos
    precio = response.data.doc.precio

    document.getElementById("nombre").value = nombre
    document.getElementById("tipo").value = id_tipo
    document.getElementById("puntos").value = puntos
    document.getElementById("precio").value = precio
  
})

async function addConsumption() {
  var response = await axios.post("http://213.195.104.135:3000/consumicion/addConsumption", {
    nombre: document.getElementById("nombre").value,
    id_tipo: document.getElementById("tipo").value,
    puntos: document.getElementById("puntos").value,
    precio: document.getElementById("precio").value
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Consumición creada",
      text: "La consumicion ha sido creada con exito."
    }).then(() => {
      window.close();
    });
  }
  else{
    swal({
      icon: "error",
      title: "Error",
      text: "Ha habido un fallo al crear la consumición.\nPuede que la consumición ya exista."
    }).then(() => {
      window.close();
    });
  }
}

async function editConsumption() {
  var response = await axios.put("http://213.195.104.135:3000/consumicion/modConsumption", {
    id: idConsumi,
    nombre: document.getElementById("nombre").value,
    id_tipo: document.getElementById("tipo").value,
    puntos: document.getElementById("puntos").value,
    precio: document.getElementById("precio").value
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Consumicion modificada",
      text: "La consumicion ha sido modificada con exito"
    }).then(() => {
      window.close();
    });
  }
  else{
    swal({
      icon: "error",
      title: "Error",
      text: "Ha habido un fallo al modificar la consumición.\nRevise los datos introducidos y pruebe de nuevo."
    }).then(() => {
      window.close();
    });
  }
}

function delConsumicion(id) {
  swal({
    title: "¿Desea eliminar la consumicion?",
    text: "La consumicion seleccionada se eliminará",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Sí",
    cancelButtonText: "No",
    reverseButtons: true
  }).then(async willDelete => {
    if (willDelete) {
      var isDeleted;
      var response = await axios.delete("http://213.195.104.135:3000/consumicion/delConsumption", {
        params: {
          id: id
        }
      });
  
      if(response.data.ok) {
        swal("Eliminado!", "la consumicion se ha eliminado!", "success");
      }
      else{
        swal("Error", "Error al eliminar la consumicion", "error");
      }      
    }
  });  
}

function cancel() {
  document.getElementById("nombre").value = nombre;
  document.getElementById("puntos").value = puntos;
  document.getElementById("precio").value = precio;
}

async function cargarTipos() {
  var response = await axios.get("http://213.195.104.135:3000/tipoConsumicion");
  var data = response.data;

  var select = document.querySelector("#tipo");
  data.forEach(tipo => {
    var opt = document.createElement('option');
    opt.value = tipo._id;
    opt.innerHTML = tipo.nombre;
    select.appendChild(opt);
  })
}
