ipc = require('electron').ipcRenderer;

var idProducto = null
var nombre = null
var descripcion = null
var tipo = null
var aforo = 0
var puntos = 0
var precio = 0

async function printDataProductos() {
  var response = await axios.get("http://213.195.104.135:3000/producto");
  
  var table = document.querySelector(".datosTabla");
  data = response.data;
  
  data.forEach(element => {
    newRow = table.insertRow(table.rows.length);
    newRow.setAttribute("id", "element" + table.rows.length);
    cellNombre = newRow.insertCell();
    cellNombre.innerHTML = element.nombre;
    cellDescripcion = newRow.insertCell();
    cellDescripcion.innerHTML = element.descripcion;
    cellPuntos = newRow.insertCell();
    cellPuntos.innerHTML = element.puntos;
    cellPrecio = newRow.insertCell();
    cellPrecio.innerHTML = element.precio + " €";
    
    cellEdit = newRow.insertCell();
    cellEdit.innerHTML =
      "<img src='../../assets/icon/edit-regular.svg' alt='edit' />";
    cellEdit.setAttribute("class", "editItem");
    cellEdit.setAttribute("onclick", "editItem('Product', '" + element._id + "')");
    cellDel = newRow.insertCell();
    cellDel.innerHTML =
      "<img src='../../assets/icon/trash-alt-regular.svg' alt='del' />";
    cellDel.setAttribute("class", "editItem");
    cellDel.setAttribute("onclick", "delProducto('" + element._id + "')");
    table.append(newRow);
  });
}

ipc.on('modProduct', async (event, id) => {
  var response = await axios.get("http://213.195.104.135:3000/producto/getProduct", {
    params: {
      id: id
    }
  });

  idProducto = response.data.doc._id
  nombre = response.data.doc.nombre
  descripcion = response.data.doc.descripcion
  tipo = response.data.doc.id_tipo
  aforo = response.data.doc.aforo
  puntos = response.data.doc.puntos
  precio = response.data.doc.precio

  document.getElementById("nombre").value = nombre
  document.getElementById("descripcion").value = descripcion
  document.getElementById("tipo").value = tipo
  document.getElementById("aforo").value = aforo
  document.getElementById("puntos").value = puntos
  document.getElementById("precio").value = precio

})

async function addProduct() {
  var response = await axios.post("http://213.195.104.135:3000/producto/addProduct", {
    nombre: document.getElementById("nombre").value,
    descripcion: document.getElementById("descripcion").value,
    tipo: document.getElementById("tipo").value,
    aforo: document.getElementById("aforo").value,
    puntos: document.getElementById("puntos").value,
    precio: document.getElementById("precio").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Producto creado",
      text: "El producto ha sido creado con exito"
    }).then(() => {
      window.close();
    });
  }
}

async function editProduct() {
  var response = await axios.put("http://213.195.104.135:3000/producto/modProduct", {
    id: idProducto,
    nombre: document.getElementById("nombre").value,
    descripcion: document.getElementById("descripcion").value,
    tipo: document.getElementById("tipo").value,
    aforo: document.getElementById("aforo").value,
    puntos: document.getElementById("puntos").value,
    precio: document.getElementById("precio").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Producto modificado",
      text: "El producto ha sido modificado con exito"
    }).then(() => {
      window.close();
    });
  }
}

function delProducto(id) {
  swal({
    title: "¿Desea eliminar el producto?",
    text: "El producto seleccionado se eliminará",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Sí",
    cancelButtonText: "No",
    reverseButtons: true
  }).then(async willDelete => {
    if (willDelete) {
      var isDeleted;
      var response = await axios.delete("http://213.195.104.135:3000/producto/delProduct", {
        params: {
          id: id
        }
      });
  
      if(response.data.ok) {
        swal("Eliminado!", "el producto se ha eliminado!", "success");
      }
      else{
        swal("Error", "error al eliminar el producto", "error");
      }      
    }
  });  
}

function cancel() {
  document.getElementById("nombre").value = nombre
  document.getElementById("descripcion").value = descripcion
  document.getElementById("aforo").value = aforo
  document.getElementById("puntos").value = puntos
  document.getElementById("precio").value = precio
}

async function cargarTipos() {
  var response = await axios.get("http://213.195.104.135:3000/tipoProducto");
  var data = response.data;

  var select = document.querySelector("#tipo");
  data.forEach(tipo => {
    var opt = document.createElement('option');
    opt.value = tipo._id;
    opt.innerHTML = tipo.nombre;
    select.appendChild(opt);
  })
}