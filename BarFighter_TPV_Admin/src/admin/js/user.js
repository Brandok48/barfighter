var ipc = require('electron').ipcRenderer;

var idUsuario = null
var nombre= null
var apellidos= null
var correo= null
var pass = null
var nick= null
var num_tel = null;
var puntos = 10;
var isPrivate = false;

async function printDataUsuarios() {
  var response = await axios.get("http://213.195.104.135:3000/usuario");
  
  var table = document.querySelector(".datosTabla");
  data = response.data;
  
  data.forEach(element => {
    newRow = table.insertRow(table.rows.length);
    newRow.setAttribute("id", "element" + table.rows.length);
    cellNombre = newRow.insertCell();
    cellNombre.innerHTML = element.nombre;
    cellApellidos = newRow.insertCell();
    cellApellidos.innerHTML = element.apellidos;
    cellNumTel = newRow.insertCell();
    cellNumTel.innerHTML = element.num_tel;
    cellCorreo = newRow.insertCell();
    cellCorreo.innerHTML = element.correo;
    cellEdit = newRow.insertCell();
    cellEdit.innerHTML =
      "<img src='../../assets/icon/edit-regular.svg' alt='edit' />";
    cellEdit.setAttribute("class", "editItem");
    cellEdit.setAttribute("onclick", "editItem('User', '" + element._id + "')");
    cellDel = newRow.insertCell();
    cellDel.innerHTML =
      "<img src='../../assets/icon/trash-alt-regular.svg' alt='del' />";
    cellDel.setAttribute("class", "editItem");
    cellDel.setAttribute("onclick", "delUsuario('" + element._id + "')");
    table.append(newRow);
  });
}

ipc.on('modUser', async (event, id) => {
    var response = await axios.get("http://213.195.104.135:3000/usuario/getUser", {
      params: {
        id: id
      }
    });

    idUsuario = response.data.doc._id
    nombre = response.data.doc.nombre
    apellidos = response.data.doc.apellidos
    correo = response.data.doc.correo
    pass = response.data.doc.pass
    nick = response.data.doc.nick
    num_tel = response.data.doc.num_tel
    puntos = response.data.doc.puntos
    isPrivate = response.data.doc.isPrivate

    document.getElementById("nombre").value = nombre
    document.getElementById("apellidos").value = apellidos
    document.getElementById("correo").value = correo
    document.getElementById("pass").value = pass
    document.getElementById("nick").value = nick
    document.getElementById("puntos").value = puntos
    document.getElementById("num_tel").value = num_tel
    document.getElementById("isPrivate").value = isPrivate
  
})

async function addUser() {
  var response = await axios.post("http://213.195.104.135:3000/usuario/addUser", {
    nombre: document.getElementById("nombre").value,
    apellidos: document.getElementById("apellidos").value,
    correo: document.getElementById("correo").value,
    pass : document.getElementById("pass").value,
    nick: document.getElementById("nick").value,
    puntos: document.getElementById("puntos").value,
    num_tel: document.getElementById("num_tel").value,
    isPrivate: document.getElementById("isPrivate").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Usuario creado",
      text: "El usuario ha sido creado con exito"
    }).then(() => {
      window.close();
    });
  }
}

async function editUser() {
  var response = await axios.put("http://213.195.104.135:3000/usuario/modUser", {
    id: idUsuario,
    nombre: document.getElementById("nombre").value,
    apellidos: document.getElementById("apellidos").value,
    correo: document.getElementById("correo").value,
    pass : document.getElementById("pass").value,
    nick: document.getElementById("nick").value,
    puntos: document.getElementById("puntos").value,
    num_tel: document.getElementById("num_tel").value,
    isPrivate: document.getElementById("isPrivate").value,
  });

  if (response.data.ok) {
    swal({
      icon: "success",
      title: "Usuario modificado",
      text: "El usuario ha sido modificado con exito"
    }).then(() => {
      window.close();
    });
  }
}

function delUsuario(id) {
  swal({
    title: "¿Desea eliminar el usuario?",
    text: "El usuario seleccionado se eliminará",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Sí",
    cancelButtonText: "No",
    reverseButtons: true
  }).then(async willDelete => {
    if (willDelete) {
      var isDeleted;
      var response = await axios.delete("http://213.195.104.135:3000/usuario/delUser", {
        params: {
          id: id
        }
      });
  
      if(response.data.ok) {
        swal("Eliminado!", "el usuario se ha eliminado!", "success");
      }
      else{
        swal("Error", "Error al eliminar el usuario", "error");
      }      
    }
  });  
}

function cancel() {
  document.getElementById("nombre").value = nombre;
  document.getElementById("apellidos").value = apellidos;
  document.getElementById("correo").value = correo;
  document.getElementById("nick").value = nick;
  document.getElementById("num_tel").value = num_tel;
  document.getElementById("puntos").value = puntos;
  document.getElementById("isPrivate").value = isPrivate;
}


