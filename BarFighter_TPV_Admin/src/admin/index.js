const swal = require("sweetalert");
const axios = require("axios").default;

const { BrowserWindow } = require("electron").remote;

adminCont = document.getElementById("adminCont");

function showAdmin() {
  $("#contenido").load("./data/content.html");
  adminCont.classList.toggle("adminSec");
  adminCont.classList.toggle("showAdminSec");
}

async function loadContent(content) {
  $("#contenido")
    .load("./data/" + content + ".html")
    .hide()
    .fadeIn("slow");

    if(content == 'users'){
      await printDataUsuarios();
    }
    if(content == 'products'){
      await printDataProductos();
    }
    if(content == 'productType'){
      await printDataTipoProductos();
    }
    if(content == 'events'){
      await printDataEventos();
    }
    if(content == 'consumption'){
      await printDataConsumiciones();
    }
    if(content == 'consumptionType'){
      await printDataTipoConsumiciones();
    }
}

async function addItem(content) {
  addItemWin = new BrowserWindow({
    width: 600,
    height: 700,
    frame: true,
    alwaysOnTop: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  //addItemWin.removeMenu();

  addItemWin.on("close", () => {
    addItemWin = null;
  });

  addItemWin.loadFile("./src/admin/add/add" + content + ".html");
  addItemWin.show();
}

async function editItem(content, id) {
  editItemWin = new BrowserWindow({
    width: 600,
    height: 700,
    frame: true,
    alwaysOnTop: true,
    webPreferences: {
      nodeIntegration: true
    }
  });


  editItemWin.webContents.on("did-finish-load", () => {
      editItemWin.webContents.send("mod" + content, id);
  });

  // editItemWin.removeMenu();

  editItemWin.on("close", () => {
    editItemWin = null;
  });

  editItemWin.loadFile("./src/admin/edit/edit" + content + ".html");
  editItemWin.show();
}


