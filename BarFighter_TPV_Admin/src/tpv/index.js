const swal = require("sweetalert");
const axios = require("axios").default;

var lblFecha = document.getElementById("fecha");
var fecha = new Date();
var ticket = document.querySelector("#cuenta tbody");
var articles = document.querySelectorAll(".article");
var sections = document.querySelector("#sections");
var consumiciones = document.querySelector("#ariclesSec");
var arrPuntos = [];

cont = 0;
selectedRows = [];

lblFecha.innerHTML = "Fecha: " + convertDate(fecha);

//Convertir Date() a 'dd/mm/yyyy'
function convertDate(inputFormat) {
  function pad(s) {
    return s < 10 ? "0" + s : s;
  }
  var d = new Date(inputFormat);

  return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join("/");
}

//select each row of table
function trSelectable(row) {
  tableRow = document.getElementById(row.id);
  tableRow.addEventListener("click", () => {
    row.classList.toggle("selected");

    row.classList.value == "selected"
      ? selectedRows.push(row.rowIndex)
      : searchAndDelete(row.rowIndex);
  });
}

//Add new row to table
async function addToTicket(id) {
  var consu = await axios.get("http://213.195.104.135:3000/consumicion/getConsumption", {
    params: {
      id: id
    }
  });

  newRow = ticket.insertRow(ticket.rows.length);
  newRow.setAttribute("id", "tableRow" + ticket.rows.length);
  cellArticle = newRow.insertCell();
  cellArticle.innerHTML = consu.data.doc.nombre;
  cellUnit = newRow.insertCell();
  cellUnit.innerHTML = "1";
  cellPrice = newRow.insertCell();
  cellPrice.innerHTML = consu.data.doc.precio + "€";
  cellPoint = newRow.insertCell();
  cellPoint.innerHTML =  consu.data.doc.puntos;
  ticket.append(newRow);

  trSelectable(newRow);

  arrPuntos.push(consu.data.doc.puntos);

  cont++;
}

//Search in array and delete
function searchAndDelete(row) {
  for (i = 0; i < selectedRows.length; i++) {
    if (row == selectedRows[i]) {
      selectedRows.splice(i, 1);
    }
  }
}

//Delete selected rows from table
function deleteFromTicket() {
  ticket = document.querySelector("#cuenta tbody");
  selectedRows.sort();
  selectedRows.reverse();

  for (i = 0; i < selectedRows.length; i++) {
    ticket.deleteRow(selectedRows[i]);
  }

  selectedRows = [];
}

//Cancelar la cuenta actual
function cancelTicket() {
  ticket = document.querySelector("#cuenta tbody");

  if (ticket.rows.length > 1) {
    swal({
      title: "¿Desea cancelar la cuenta?",
      text: "La cuenta actual se eliminará",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("Eliminada!", "La cuenta se ha eliminado!", "success").then(() => {
          document.location.reload(true);
        });
      }
    });
  } else {
    swal({
      title: "Error de cuenta",
      text: "La cuenta actual aún no tiene artículos",
      icon: "error",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
    });
  }
}

//Pagar la cuenta
function payTicket() {
  if (ticket.rows.length > 1) {
    swal({
      title: "¿Desea finalizar la cuenta?",
      text: "La cuenta actual se finalizará",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, pay it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal({
          text: 'Ingrese el nick del usuario',
          content: "input",
          button: {
            text: "Buscar!",
            closeModal: false,
          },
        }).then(nick => {
          if (!nick) {
            swal({
              title: "Error",
              text: "Vuelva a intentarlo",
              icon: "error",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel!",
              reverseButtons: true,
            });
          }
          else{
            var response = axios.get("http://213.195.104.135:3000/usuario/getUserByNick", {
              params: {
                nick: nick
              }
            });

            setTimeout(function() {
              response.then(user => {
                if(!user.data.ok) {
                  swal({
                    title: "Error",
                    text: "El usuario no existe",
                    icon: "error",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                  });
                }
                else{
                  var response = axios.put("http://213.195.104.135:3000/usuario/modUser", {
                    id: user.data.doc._id,
                    nombre: user.data.doc.nombre,
                    apellidos: user.data.doc.apellidos,
                    correo: user.data.doc.correo,
                    pass : user.data.doc.pass,
                    nick: user.data.doc.nick,
                    puntos: user.data.doc.puntos + arrPuntos.reduce((a, b) => a + b, 0),
                    num_tel: user.data.doc.num_tel,
                    isPrivate: user.data.doc.isPrivate,
                  });

                  swal("Pagada!", "La cuenta ha sido pagada!", "success").then(() => {
                    document.location.reload(true);
                  });
                }
              })
            }, 3000)
            
          }
        })
      }
    });
  } else {
    swal({
      title: "Error de cuenta",
      text: "La cuenta actual aún no tiene artículos",
      icon: "error",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
    });
  }
}

async function cargarDatos() {
  var tipos = await axios.get("http://213.195.104.135:3000/tipoConsumicion/");
  pintarTipos(tipos.data);
}

function pintarTipos(tipos) {
  tipos.forEach((section) => {
    var newDiv = document.createElement("div");
    newDiv.id = section._id;
    newDiv.className = "flex artSection";
    newDiv.innerHTML = section.nombre;
    newDiv.setAttribute(
      "onclick",
      "cargarConsumiciones('" + section._id + "')"
    );

    sections.appendChild(newDiv);
  });
}

async function cargarConsumiciones(id) {
  var consumis = await axios.get("http://213.195.104.135:3000/consumicion/getConsumptionByType", {
    params: {
      id: id
    }
  });
  pintarConsumis(consumis.data);
}

function pintarConsumis(consumis) {
  consumiciones.innerHTML = '';

  consumis.doc.forEach((consumi) => {
    var newDiv = document.createElement("div");
    newDiv.id = consumi._id;
    newDiv.className = "flex artSection article";
    newDiv.innerHTML = consumi.nombre;
    newDiv.setAttribute("onclick", "addToTicket('" + consumi._id + "')");

    consumiciones.appendChild(newDiv);
  });
}
