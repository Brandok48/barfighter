var tipoConsumicion = require("../models/tipoConsumicion");

function getTipoConsumiciones(req, res) {
  tipoConsumicion.find().exec(function (err, doc) {
    res.send(doc);
  });
}

function getTipoConsumicion(req, res) {
  var { id } = req.query;

  tipoConsumicion.findById(id).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      console.log("El tipo de consumicion no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc
      });
    }
  });
}

function addTipoConsumicion(req, res) {
  var { nombre } = req.body;

  tipoConsumicion.findOne({ nombre: nombre }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      var typeConsumption = new tipoConsumicion({
        nombre: nombre
      });

      typeConsumption.save((err, tipoConsumptionGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al crear el tipo de consumption",
            errors: err
          });
        }
      });

      res.status(200).json({
        ok: true,
        typeConsumption
      });
    } else {
      return res.status(201).json({
        ok: false,
        typeConsumption: "El consumicion ya existe"
      });
    }
  });
}

function modTipoConsumicion(req, res) {
  var { id, nombre } = req.body;

  tipoConsumicion.findOne({ _id: id }).exec(function (err, typeConsumption) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (typeConsumption != null) {
      (typeConsumption.nombre = nombre);

      typeConsumption.save((err, tipoConsumptionGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al modificar el consumption",
            errors: err
          });
        }

        res.status(200).json({
          ok: true,
          typeConsumption
        });
      });
    } else {
      return res.status(400).json({
        ok: false,
        typeConsumption: "El tipo de consumicion no existe"
      });
    }
  });
}

function delTipoConsumicion(req, res) {
  var { id } = req.query;

  tipoConsumicion.findByIdAndRemove(id, (err, tipoconsumptionDel) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error al borrar el tipo de consumption",
        errors: err
      });
    }

    if (!tipoconsumptionDel) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un tipo de consumicion con ese ID"
      });
    }

    res.status(200).json({
      ok: true,
      typeConsumption: tipoconsumptionDel
    });
  });
}

module.exports = {
  getTipoConsumicion,
  getTipoConsumiciones,
  addTipoConsumicion,
  modTipoConsumicion,
  delTipoConsumicion
};
