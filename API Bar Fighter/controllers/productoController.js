var producto = require("../models/producto");

function getProductos(req, res) {
  producto.find().exec(function (err, doc) {
    res.send(doc);
  });
}

function getProducto(req, res) {
  var { id } = req.query;

  producto.findById(id).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      console.log("El producto no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc
      });
    }
  });
}

function addProducto(req, res) {
  var { nombre, descripcion, tipo, aforo, puntos, precio } = req.body;
  
  producto.findOne({ nombre: nombre }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      var product = new producto({
        nombre: nombre,
        descripcion: descripcion,
        id_tipo: tipo,
        aforo: aforo,
        puntos: puntos,
        precio: precio
      });

      product.save((err, productoGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al crear el producto",
            errors: err
          });
        }
      });

      res.status(200).json({
        ok: true,
        product
      });
    } else {
      return res.status(201).json({
        ok: false,
        product: "El producto ya existe"
      });
    }
  });
}

function modProducto(req, res) {
  var { id, nombre, descripcion, id_tipo, aforo, puntos, precio } = req.body;

  producto.findOne({ _id: id }).exec(function (err, product) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (product != null) {
      (product.nombre = nombre),
        (product.descripcion = descripcion),
        (product.id_tipo = id_tipo),
        (product.aforo = aforo),
        (product.puntos = puntos),
        (product.precio = precio);

      product.save((err, productoGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al modificar el producto",
            errors: err
          });
        }

        res.status(200).json({
          ok: true,
          product
        });
      });
    } else {
      return res.status(400).json({
        ok: false,
        product: "El producto no existe"
      });
    }
  });
}

function delProducto(req, res) {
  var { id } = req.query;

  producto.findByIdAndRemove(id, (err, productoDel) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error al borrar el producto",
        errors: err
      });
    }

    if (!productoDel) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un producto con ese ID"
      });
    }

    res.status(200).json({
      ok: true,
      product: productoDel
    });
  });
}

module.exports = {
  getProducto,
  getProductos,
  addProducto,
  modProducto,
  delProducto
};
