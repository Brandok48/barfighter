var usuario = require("../models/usuario");

function getUsuarios(req, res) {
  usuario.find().exec(function (err, doc) {
    res.send(doc);
  });
}

function getUsuario(req, res) {
  var { id } = req.query;

  usuario.findById(id).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      console.log("El usuario no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc
      });
    }
  });
}

function getUsuarioPorNick(req, res) {
  var { nick } = req.query;

  usuario.findOne({ nick: nick }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      res.status(200).json({
        ok: false
      });
    } else {
      res.status(200).json({
        ok: true,
        doc
      });
    }
  });
}

function addUser(req, res) {
  var {
    nombre,
    apellidos,
    correo,
    pass,
    nick,
    puntos,
    num_tel,
    isPrivate
  } = req.body;

  usuario.findOne({ nick: nick }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      var user = new usuario({
        nombre: nombre,
        apellidos: apellidos,
        correo: correo,
        pass: pass,
        nick: nick,
        puntos: puntos,
        num_tel: num_tel,
        isPrivate: isPrivate
      });

      user.save((err, usuarioGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al crear el usuario",
            errors: err
          });
        }
      });

      res.status(200).json({
        ok: true,
        user
      });
    } else {
      return res.status(201).json({
        ok: false,
        user: "El usuario ya existe"
      });
    }
  });
}

function modUsuario(req, res) {
  var {
    id,
    nombre,
    apellidos,
    correo,
    pass,
    nick,
    puntos,
    num_tel,
    isPrivate
  } = req.body;

  console.log(id)

  usuario.findOne({ _id: id }).exec(function (err, user) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (user != null) {
      (user.nombre = nombre),
        (user.apellidos = apellidos),
        (user.correo = correo),
        (user.pass = pass),
        (user.nick = nick),
        (user.puntos = puntos),
        (user.num_tel = num_tel),
      user.isPrivate = isPrivate;

      user.save((err, usuarioGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al modificar el usuario",
            errors: err
          });
        }

        res.status(200).json({
          ok: true,
          user
        });
      });
    } else {
      return res.status(400).json({
        ok: false,
        user: "El usuario no existe"
      });
    }
  });
}

function delUsuario(req, res) {
  var { id } = req.query;

  usuario.findByIdAndRemove(id, (err, usuarioDel) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error al borrar el usuario",
        errors: err
      });
    }

    if (!usuarioDel) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un usuario con ese ID"
      });
    }

    res.status(200).json({
      ok: true,
      user: usuarioDel
    });
  });
}

module.exports = {
  getUsuarios,
  getUsuario,
  getUsuarioPorNick,
  addUser,
  delUsuario,
  modUsuario
};
