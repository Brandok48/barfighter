var Reserva = require("../models/reserva");

function getReservas(req, res) {
    Reserva.find().exec(function (err, doc) {
    res.send(doc);
  });
}

function getReserva(req, res) {
  var { id } = req.query;

  Reserva.findOne({ _id: id }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      console.log("La reserva no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc
      });
    }
  });
}

function addReserva(req, res) {
  var { id_producto, id_usuario, fecha } = req.body;

    var reserva = new Reserva({
      id_producto: id_producto,
      id_usuario: id_usuario,
      fecha: fecha
    });

    console.log(new Date())

    reserva.save((err, reservaGuardado) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          mensaje: "Error al crear la reserva",
          errors: err
        });
      }
    });

    res.status(200).json({
      ok: true,
      reserva
    });
}

function modReserva(req, res) {
  var { id, id_producto, id_usuario, fecha } = req.body;

  Reserva.findOne({ _id: id }).exec(function (err, reserva) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (reserva != null) {
      (reserva.id_producto = id_producto),
        (reserva.id_usuario = id_usuario),
        (reserva.fecha = fecha);

        reserva.save((err, reservaGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al modificar la reserva",
            errors: err
          });
        }

        res.status(200).json({
          ok: true,
          reserva
        });
      });
    } else {
      return res.status(400).json({
        ok: false,
        evento: "La reserva no existe"
      });
    }
  });
}

function delReserva(req, res) {
  var { id } = req.query;

  Reserva.findByIdAndRemove(id, (err, reservaDel) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error al borrar la reserva",
        errors: err
      });
    }

    if (!reservaDel) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe una reserva con ese ID"
      });
    }

    res.status(200).json({
      ok: true,
      reserva: reservaDel
    });
  });
}
module.exports = {
  getReserva,
  getReservas,
  addReserva,
  modReserva,
  delReserva
};
