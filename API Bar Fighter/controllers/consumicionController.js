var consumicion = require("../models/consumicion");

function getConsumiciones(req, res) {
  consumicion.find().exec(function (err, doc) {
    res.send(doc);
  });
}

function getConsumicion(req, res) {
  var { id } = req.query;

  consumicion.findById(id).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err,
      });
    }

    if (doc == null) {
      console.log("La consumicion no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc,
      });
    }
  });
}

function getConsumicionTipo(req, res) {
  var { id } = req.query;
  
  consumicion.find({ id_tipo: id }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err,
      });
    }

    if (doc == null) {
      console.log("La consumicion no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc,
      });
    }
  });
}

function addConsumicion(req, res) {
  var { nombre, id_tipo, puntos, precio } = req.body;
  
  consumicion.findOne({ nombre: nombre }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err,
      });
    }

    if (doc == null) {
      var consumption = new consumicion({
        nombre: nombre,
        id_tipo: id_tipo,
        puntos: puntos,
        precio: precio,
      });

      consumption.save((err, consumicionGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al crear la consumicion",
            errors: err,
          });
        }
      });

      res.status(200).json({
        ok: true,
        consumption,
      });
    } else {
      return res.status(201).json({
        ok: false,
        product: "La consumicion ya existe",
      });
    }
  });
}

function modConsumicion(req, res) {
    var { id, nombre, id_tipo, puntos, precio } = req.body;

  consumicion.findOne({ _id: id }).exec(function (err, consumption) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err,
      });
    }

    if (consumption != null) {
      (consumption.nombre = nombre),
        (consumption.id_tipo = id_tipo),
        (consumption.puntos = puntos),
        (consumption.precio = precio);

        consumption.save((err, consumicionGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al modificar la consumicion",
            errors: err,
          });
        }

        res.status(200).json({
          ok: true,
          consumption,
        });
      });
    } else {
      return res.status(400).json({
        ok: false,
        consumption: "La consumicion existe",
      });
    }
  });
}

function delConsumicion(req, res) {
  var { id } = req.query;

  consumicion.findByIdAndRemove(id, (err, consumicionDel) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error al borrar la consumicion",
        errors: err,
      });
    }

    if (!consumicionDel) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe una consumicion con ese ID",
      });
    }

    res.status(200).json({
      ok: true,
      consumption: consumicionDel,
    });
  });
}

module.exports = {
  getConsumicion,
  getConsumiciones,
  getConsumicionTipo,
  addConsumicion,
  modConsumicion,
  delConsumicion,
};
