var Evento = require("../models/evento");

function getEventos(req, res) {
    Evento.find().exec(function (err, doc) {
    res.send(doc);
  });
}

function getEvento(req, res) {
  var { id } = req.query;

  Evento.findOne({ _id: id }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      console.log("El evento no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc
      });
    }
  });
}

function addEvento(req, res) {
  var { nombre, descripcion, fecha } = req.body;

  Evento.findOne({ nombre: nombre }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      var evento = new Evento({
        nombre: nombre,
        descripcion: descripcion,
        fecha: fecha,
        asistentes: []
      });

      evento.save((err, eventoGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al crear el evento",
            errors: err
          });
        }
      });

      res.status(200).json({
        ok: true,
        evento
      });
    } else {
      return res.status(201).json({
        ok: false,
        evento: "El evento ya existe"
      });
    }
  });
}

function modEvento(req, res) {
  var { id, nombre, descripcion, fecha } = req.body;

  Evento.findOne({ _id: id }).exec(function (err, evento) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (evento != null) {
      (evento.nombre = nombre),
        (evento.descripcion = descripcion),
        (evento.fecha = fecha),
        (evento.asistentes = evento.asistentes);

      evento.save((err, eventoGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al modificar el evento",
            errors: err
          });
        }

        res.status(200).json({
          ok: true,
          evento
        });
      });
    } else {
      return res.status(400).json({
        ok: false,
        evento: "El evento no existe"
      });
    }
  });
}

function delEvento(req, res) {
  var { id } = req.query;

  Evento.findByIdAndRemove(id, (err, eventoDel) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error al borrar el evento",
        errors: err
      });
    }

    if (!eventoDel) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un evento con ese ID"
      });
    }

    res.status(200).json({
      ok: true,
      evento: eventoDel
    });
  });
}
module.exports = {
  getEvento,
  getEventos,
  addEvento,
  modEvento,
  delEvento
};
