var tipoProducto = require("../models/tipoProducto");

function getTipoProductos(req, res) {
  tipoProducto.find().exec(function (err, doc) {
    res.send(doc);
  });
}

function getTipoProducto(req, res) {
  var { id } = req.query;

  tipoProducto.findById(id).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      console.log("El tipo de producto no existe");
    } else {
      res.status(200).json({
        ok: true,
        doc
      });
    }
  });
}

function addTipoProducto(req, res) {
  var { nombre } = req.body;
  console.log(req.body)

  tipoProducto.findOne({ nombre: nombre }).exec(function (err, doc) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (doc == null) {
      var typeProduct = new tipoProducto({
        nombre: nombre
      });

      typeProduct.save((err, tipoProductoGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al crear el tipo de producto",
            errors: err
          });
        }
      });

      res.status(200).json({
        ok: true,
        typeProduct
      });
    } else {
      return res.status(201).json({
        ok: false,
        typeProduct: "El producto ya existe"
      });
    }
  });
}

function modTipoProducto(req, res) {
  var { id, nombre } = req.body;

  tipoProducto.findOne({ _id: id }).exec(function (err, typeProduct) {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error de base de datos",
        errors: err
      });
    }

    if (typeProduct != null) {
      (typeProduct.nombre = nombre);

      typeProduct.save((err, tipoProductoGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: "Error al modificar el producto",
            errors: err
          });
        }

        res.status(200).json({
          ok: true,
          typeProduct
        });
      });
    } else {
      return res.status(400).json({
        ok: false,
        typeProduct: "El tipo de producto no existe"
      });
    }
  });
}

function delTipoProducto(req, res) {
  var { id } = req.query;

  tipoProducto.findByIdAndRemove(id, (err, tipoProductoDel) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error al borrar el tipo de producto",
        errors: err
      });
    }

    if (!tipoProductoDel) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un tipo de producto con ese ID"
      });
    }

    res.status(200).json({
      ok: true,
      typeProduct: tipoProductoDel
    });
  });
}

module.exports = {
  getTipoProducto,
  getTipoProductos,
  addTipoProducto,
  modTipoProducto,
  delTipoProducto
};
