var express = require ('express')
var app = express()
var CTipoProducto = require('../controllers/tipoProductoController.js');

var rateLimit = require('express-rate-limit')

const limite = rateLimit({
    max: 100,
    windowsMs: 60*60*1000 // milisegundos = 1 hora
})

app.get('/', CTipoProducto.getTipoProductos);
app.get('/getTypeProduct', CTipoProducto.getTipoProducto);
app.post('/addTypeProduct', CTipoProducto.addTipoProducto);
app.put('/modTypeProduct', CTipoProducto.modTipoProducto);
app.delete('/delTypeProduct', CTipoProducto.delTipoProducto);

module.exports = app;