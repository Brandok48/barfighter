var express = require ('express')
var app = express()
var CConsumicion = require('../controllers/consumicionController.js');

var rateLimit = require('express-rate-limit')

const limite = rateLimit({
    max: 100,
    windowsMs: 60*60*1000 // milisegundos = 1 hora
})

app.get('/', CConsumicion.getConsumiciones);
app.get('/getConsumption', CConsumicion.getConsumicion);
app.get('/getConsumptionByType', CConsumicion.getConsumicionTipo);
app.post('/addConsumption', CConsumicion.addConsumicion);
app.put('/modConsumption', CConsumicion.modConsumicion);
app.delete('/delConsumption', CConsumicion.delConsumicion);

module.exports = app;