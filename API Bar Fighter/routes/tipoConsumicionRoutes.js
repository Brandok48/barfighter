var express = require ('express')
var app = express()
var CTipoConsumicion = require('../controllers/tipoConsumicionController.js');

var rateLimit = require('express-rate-limit')

const limite = rateLimit({
    max: 100,
    windowsMs: 60*60*1000 // milisegundos = 1 hora
})

app.get('/', CTipoConsumicion.getTipoConsumiciones);
app.get('/getTypeConsumption', CTipoConsumicion.getTipoConsumicion);
app.post('/addTypeConsumption', CTipoConsumicion.addTipoConsumicion);
app.put('/modTypeConsumption', CTipoConsumicion.modTipoConsumicion);
app.delete('/delTypeConsumption', CTipoConsumicion.delTipoConsumicion);

module.exports = app;