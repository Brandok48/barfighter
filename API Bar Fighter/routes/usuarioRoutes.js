var express = require ('express');
var app = express();
var CUsuario = require('../controllers/usuarioController.js');

var rateLimit = require('express-rate-limit');

const limite = rateLimit({
    max: 100,
    windowsMs: 60*60*1000 // milisegundos = 1 hora
});

app.get('/', CUsuario.getUsuarios);
app.get('/getUser', CUsuario.getUsuario);
app.get('/getUserByNick', CUsuario.getUsuarioPorNick);
app.post('/addUser', CUsuario.addUser);
app.put('/modUser', CUsuario.modUsuario);
app.delete('/delUser', CUsuario.delUsuario);

module.exports = app;