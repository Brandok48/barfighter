var express = require ('express')
var app = express()
var CProducto = require('../controllers/productoController.js');

var rateLimit = require('express-rate-limit')

const limite = rateLimit({
    max: 100,
    windowsMs: 60*60*1000 // milisegundos = 1 hora
})

app.get('/', CProducto.getProductos);
app.get('/getProduct', CProducto.getProducto);
app.post('/addProduct', CProducto.addProducto);
app.put('/modProduct', CProducto.modProducto);
app.delete('/delProduct', CProducto.delProducto);

module.exports = app;