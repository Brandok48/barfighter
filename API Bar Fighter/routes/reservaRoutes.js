var express = require ('express')
var app = express()
var CReserva = require('../controllers/ReservaController.js')

var rateLimit = require('express-rate-limit')

const limite = rateLimit({
    max: 100,
    windowsMs: 60*60*1000 // milisegundos = 1 hora
})


app.get('/', CReserva.getReservas);
app.get('/getReservation', CReserva.getReserva);
app.post('/addReservation', CReserva.addReserva);
app.put('/modReservation', CReserva.modReserva);
app.delete('/delReservation', CReserva.delReserva);

module.exports = app;

