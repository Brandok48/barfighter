var express = require ('express')
var app = express()
var CEvento = require('../controllers/eventoController.js')

var rateLimit = require('express-rate-limit')

const limite = rateLimit({
    max: 100,
    windowsMs: 60*60*1000 // milisegundos = 1 hora
})


app.get('/', CEvento.getEventos);
app.get('/getEvent', CEvento.getEvento);
app.post('/addEvent', CEvento.addEvento);
app.put('/modEvent', CEvento.modEvento);
app.delete('/delEvent', CEvento.delEvento);

module.exports = app;

