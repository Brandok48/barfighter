var mongoose = require('mongoose')
var Schema = mongoose.Schema
var eventoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    descripcion: { type: String, required: [true, 'La descripcion es obligatoria'] },
    fecha: { type: Date },
    asistentes: { type: [Schema.Types.ObjectId], ref: 'usuario'},
})

module.exports = mongoose.model('Evento',eventoSchema)