var mongoose = require('mongoose')
var Schema = mongoose.Schema

var tipoProductoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
})

module.exports = mongoose.model('TipoProducto', tipoProductoSchema)