var mongoose = require('mongoose')
var Schema = mongoose.Schema

var consumicionSchema = new Schema({
  nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
  id_tipo: { type: Schema.Types.ObjectId, ref: 'tipoConsumicion'},
  puntos: { type: Number },
  precio: { type: Number },
})

module.exports = mongoose.model('Consumicion', consumicionSchema)