var mongoose = require('mongoose')
var Schema = mongoose.Schema
var reservaSchema = new Schema({
    id_producto: { type: Schema.Types.ObjectId, ref: 'producto'},
    id_usuario : { type: Schema.Types.ObjectId, ref: 'usuario'},
    fecha: Date,
})

module.exports = mongoose.model('Reserva', reservaSchema)