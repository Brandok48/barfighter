var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    apellidos: { type: String },
    correo: { type: String, required: [true, 'El correo es obligatorio'] },
    pass: { type: String, required: [true, 'La constraseña es obligatoria'] },
    nick: { type: String, required: [true, 'El nick es obligatorio'] },
    puntos: { type: Number },
    num_tel: { type: String, required: [true, 'El número de teléfono es obligatorio'] },
    isPrivate: { type: Boolean },
});

module.exports = mongoose.model('Usuario', usuarioSchema)