var mongoose = require('mongoose')
var Schema = mongoose.Schema

var productoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    descripcion: { type: String, required: [true, 'La descripcion es obligatoria'] },
    id_tipo: { type: Schema.Types.ObjectId, ref: 'tipoProducto'},
    aforo: { type: Number },
    puntos: { type: Number },
    precio: { type: Number },
})

module.exports = mongoose.model('Producto', productoSchema)