var express = require("express"),
  bodyParser = require("body-parser"),
  mong = require("mongoose");

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// CORS
var cors = require("cors");
app.use(cors());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "DELETE, PUT, GET, POST");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

mong.connection.openUri("mongodb://localhost:27017/BarFighter", (err, res) => {
  if (err) {
    console.log("ERROR: connecting to Database. " + err);
  }
});

// RUTAS
const usuarioRoutes = require("./routes/usuarioRoutes");
const productoRoutes = require("./routes/productoRoutes");
const tipoProductoRoutes = require("./routes/tipoProductoRoutes");
const tipoEventoRoutes = require("./routes/eventoRoutes");
const consumicionRoutes = require("./routes/consumicionRoutes");
const tipoConsumicionRoutes = require("./routes/tipoConsumicionRoutes");
const reservaRoutes = require("./routes/reservaRoutes");

app.use("/usuario", usuarioRoutes);
app.use("/producto", productoRoutes);
app.use("/tipoProducto", tipoProductoRoutes);
app.use("/evento", tipoEventoRoutes);
app.use("/consumicion", consumicionRoutes);
app.use("/tipoConsumicion", tipoConsumicionRoutes);
app.use("/reserva", reservaRoutes);

//limitantes y seguridad
//app.use(express.json({ limit: '10kb' }));

const PORT = 3000;
app.listen(PORT, () => {
  console.log("API corriendo en el puerto " + PORT);
});
